#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <smlib>
#include <sm_jail_redie>

#define PLUGIN_VERSION "b1.2.2"

Handle HideAttackerTs = INVALID_HANDLE;
Handle HideAttackerCt = INVALID_HANDLE;

bool hide_cts;
bool hide_ts;
bool g_bHook;

int g_iTV;

public Plugin:myinfo =
{
	name = "[CS:GO] SM Hide Attacker",
	author = "Franc1sco && ShaRen",
	description = "Hide attackers",
	version = PLUGIN_VERSION,
	url = "Servers-Info.Ru"
};

public OnPluginStart()
{

	CreateConVar("sm_hideattacker_version", PLUGIN_VERSION, "version", FCVAR_SPONLY|FCVAR_NOTIFY);
	HideAttackerTs = CreateConVar("sm_hideattacker_ts", "1", "hide Ts attackers");
	HideAttackerCt = CreateConVar("sm_hideattacker_ct", "0", "hide CTs attackers");
	HookEvent("round_start", event_round_start, EventHookMode_Pre);
	HookEvent("round_end", event_round_end, EventHookMode_Pre);
	HookConVarChange(HideAttackerCt, OnCVarChange);
	HookConVarChange(HideAttackerTs, OnCVarChange);
}

public OnCVarChange(Handle:convar_hndl, const String:oldValue[], const String:newValue[])
{
	GetCVars();
}

public OnConfigsExecuted()
{
	GetCVars();
}

// Get new values of cvars if they has being changed
public GetCVars()
{
	hide_cts = GetConVarBool(HideAttackerCt);
	hide_ts = GetConVarBool(HideAttackerTs);

}

public Action event_round_end(Event e, const String:name[], bool:dontBroadcast)
{
	if(g_bHook) {
		UnhookEvent("player_death", event_Death, EventHookMode_Pre);
		g_bHook = false;
	}
}

public Action event_round_start(Event e, const String:name[], bool:dontBroadcast)
{
	if(!g_bHook) {
		HookEvent("player_death", event_Death, EventHookMode_Pre);
		g_bHook = true;
	}
	for (int i=1; i<=MaxClients; i++)
		if(IsClientInGame(i) && IsClientSourceTV(i)) {
			g_iTV = i;
			return Plugin_Continue;
		}
	return Plugin_Continue;
}
public Action event_Death(Event e, const String:name[], bool:dontBroadcast)
{
	if (!dontBroadcast) {
		int attacker =	GetClientOfUserId(e.GetInt("attacker"));
		if (!attacker)			return Plugin_Continue;
		int userid = GetClientOfUserId(e.GetInt("userid"));
		int iTeamAttacker = GetClientTeam(attacker);
		int iTeamUserId = GetClientTeam(userid);
		int iAliveCT, iAliveT;
		Team_GetAliveClientCounts(iAliveCT, iAliveT);
		if ((iTeamAttacker == 2 && iTeamUserId == 3 && hide_ts && iAliveCT > 0 && iAliveT > 0) || (iTeamAttacker == 3 && iTeamUserId == 2 && hide_cts)) {
			if (g_iTV && IsClientInGame(g_iTV) && IsClientSourceTV(g_iTV))
				e.SetInt("attacker", GetClientUserId(g_iTV));
			else e.SetInt("attacker", GetClientUserId(userid));
		}
	}
	return Plugin_Continue;
}